* This is the Tremors Suggestion Template!

* When finished with the suggestion **delete all lines starting with "0"**

[SUGGESTION TYPE]
0 This can be **ITEM, ARMOR, MOB, TALISMAN, REFORGE, ENCHANT, ACHEIVEMENT, TEXTURE, TOWNY** and **MISC**

[SUGGESTION DESCRIPTION]
0 The Suggestion itself

[REASON]
0 This is optional **[DELETE IF NOT USED]**, but it makes it more likely to be added

[REFERENCE]
0 Also optional **[DELETE IF NOT USED]**, but helps me imagine your idea
0 Things like **images, videos or an article**
